package main

import (
	"fmt"
)

func main() {
	/*
	  abcde
	
	  a  e
	  b d
	  c
	*/
	s := skip(3, 1, true)
	fmt.Printf("%v\n", s)
	r := convert("abcde", 3)
	fmt.Printf("%v\n", r)
}

func convert(s string, numRows int) string {
    // gap between initial letters in result 1st row
    // 3 rows - 3 extra letters
    // 4 rows - 5 extra letters
    // 5 rows - 7 extra letters
    // 6 rows - 9 extra letters
    // => letters = (hight - 2) * 2 + 1
    
    // in N's row same as the first one, but it just becomes -1 (e.g. 1st is 4 rows, then 2nd - 3 rows)
    // and need to switch direction - up or down. with example of 4 rows, for the 2nd row: dow
    // start with down, then up, then down & etc. 
    
    
    // stop once reached the end of original string
    // and start filling in the next line
    totalLength := len(s)
    if totalLength == 0 {
        return ""
    }
    result := make([]rune, len(s))
    input := []rune(s)
    result[0] = input[0]
    // all left to the inputIndex - already processed
    inputIndex := 1
    // last inserted position in result string
    resultIndex := 1
    for l := 1; l <= numRows; l++ {
        // direction up or down, used to count skipped letters
        lastMoveUp := true
fmt.Printf("skip %v\n", skip)
        skipCount := 0
        for resultIndex + skipCount < totalLength && inputIndex < totalLength {
            skipCount := skip(numRows, l, lastMoveUp)
            resultIndex += skipCount
            result[resultIndex] = input[inputIndex]
            inputIndex++
            lastMoveUp = !lastMoveUp
        }
    }
    return string(result)
}

func skip(totalNumRows int, currentRowNum int, lastMoveUp bool) (result int) {
    hight := 0
    if lastMoveUp {
        hight = totalNumRows - currentRowNum + 1
    } else {
        hight = currentRowNum
    }
    return (hight - 2) * 2 + 1
}