package main // module largest_island

import "fmt" /*
	11234
	12223
	22444
	31442
*/

// Cell - a board cell
type Cell struct {
	X     int32
	Y     int32
	Color int
}

var theMap = [][]int{
	{3, 1, 4, 4, 2},
	{2, 2, 4, 4, 4},
	{1, 2, 2, 2, 3},
	{1, 1, 2, 3, 4},
}

func main() {
	result := []*Cell{}
	if len(theMap) == 0 || len(theMap[0]) == 0 {
		return
	}
	countedCells := map[uint32]struct{}{}
	current := GetStartingCell(theMap)
	totalLength := len(theMap) * len(theMap[0])
	nextIndex := 0
	for len(countedCells) < totalLength {
		if !current.WasCounted(countedCells) {
			currentIsland := []*Cell{current}
			GrowIsland(current, nil, &currentIsland, countedCells)
			if len(currentIsland) > len(result) {
				result = currentIsland
			}
		}
		nextIndex++
		if nextIndex >= totalLength {
			break
		}
		y := int32(nextIndex / len(theMap[0]))
		x := int32(nextIndex % len(theMap[0]))
		current = &Cell{X: x, Y: y, Color: theMap[y][x]}
	}
	for _, c := range result {
		fmt.Printf("x=%d y=%d color=%d\n", c.X, c.Y, c.Color)
	}
}

// GrowIsland - extend island from the given cell
func GrowIsland(cell *Cell, arrivedFrom *Cell, island *[]*Cell, countedCells map[uint32]struct{}) {
	neighbours := cell.GetSameColorNeighbours(theMap)
	cell.MarkAsCounted(countedCells)
	for _, c := range neighbours {
		if !ContainsCell(island, c) && !c.WasCounted(countedCells) && (arrivedFrom == nil || !c.Equals(*arrivedFrom)) {
			*island = append(*island, c)
			GrowIsland(c, cell, island, countedCells)
		}
	}
}

// ContainsCell - checks if island contains the cell
func ContainsCell(island *[]*Cell, cell *Cell) bool {
	for _, c := range *island {
		if c.Equals(*cell) {
			return true
		}
	}
	return false
}

// GetStartingCell - returns 0,0 (a begining) cell
func GetStartingCell(theMap [][]int) (result *Cell) {
	return &Cell{
		X:     0,
		Y:     0,
		Color: theMap[0][0],
	}
}

// Equals - checks if two cells are the same
func (c Cell) Equals(another Cell) bool {
	return c.X == another.X && c.Y == another.Y
}

// WasCounted - indicates if the cell have been seen before
func (c Cell) WasCounted(countedCells map[uint32]struct{}) bool {
	_, ok := countedCells[uint32(c.X)+uint32(c.Y)<<16]
	return ok
}

// MarkAsCounted - marks the cell as been seen before
func (c Cell) MarkAsCounted(countedCells map[uint32]struct{}) {
	countedCells[uint32(c.X)+uint32(c.Y)<<16] = struct{}{}
}

// GetSameColorNeighbours - returns neighbours of the same color as the cell
func (c Cell) GetSameColorNeighbours(theMap [][]int) (result []*Cell) {
	all := c.GetNeighbours(theMap)
	for _, cell := range all {
		if cell.Color == c.Color {
			result = append(result, cell)
		}
	}
	return
}

// GetNeighbours - returns top, right, bottom & left neighbours
func (c Cell) GetNeighbours(theMap [][]int) (result []*Cell) {
	mapHeight := int32(len(theMap))
	if mapHeight == 0 {
		return
	}
	mapLength := int32(len(theMap[0]))

	topX := c.X
	topY := c.Y + 1
	if topY < mapHeight {
		result = append(result, &Cell{
			X:     topX,
			Y:     topY,
			Color: theMap[topY][topX],
		})
	}

	rightX := c.X + 1
	rightY := c.Y
	if rightX < mapLength {
		result = append(result, &Cell{
			X:     rightX,
			Y:     rightY,
			Color: theMap[rightY][rightX],
		})
	}

	bottomX := c.X
	bottomY := c.Y - 1
	if bottomY >= 0 {
		result = append(result, &Cell{
			X:     bottomX,
			Y:     bottomY,
			Color: theMap[bottomY][bottomX],
		})
	}

	leftX := c.X - 1
	leftY := c.Y
	if leftX >= 0 {
		result = append(result, &Cell{
			X:     leftX,
			Y:     leftY,
			Color: theMap[leftY][leftX],
		})
	}
	return
}
