package main

import (
	"fmt"
)

func main() {
	number := int64(600851475143)
	currentPrime := int64(1)
	lastPrime := int64(1)
	for number > 1 {
		currentPrime, _ = getNextPrime(currentPrime)
		if number%currentPrime == 0 {
			number = number / currentPrime
			lastPrime = currentPrime
			currentPrime = 1
		}
	}

	fmt.Printf("largest prime factor: %v", lastPrime)
}

func getNextPrime(originalPrime int64) (int64, bool) {
	int64max := int64(^uint64(0) >> 1)
	isPrime := true
	for nextPrime := originalPrime + 1; nextPrime < int64max; nextPrime++ {
		isPrime = true
		for i := int64(2); i < nextPrime; i++ {
			if nextPrime%i == 0 {
				isPrime = false
				break
			}
		}
		if isPrime {
			return nextPrime, true
		}
	}
	return -1, false
}
