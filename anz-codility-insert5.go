/*
	write a function Solution(N int) int, so given N = 347 -> 5347; 743 -> 7543
	-456 -> -4556
	insert 5 into position to returns maximum possible
*/
package main

func main() {
	r := Solution(347)
}

func Solution(N int) int {
	if N == 0 {
		return 50
	}
	positive := false
	if N > 0 {
		positive = true
	}
	str := strconv.Itoa(N)
	var result string
	if positive {
		position := len(str)
		matchFound := false
		for _, r := range str {
			s := string(r)
			i, _ := strconv.Atoi(s)
			if !matchFound && i <= 5 {
				matchFound = true
				result = result + "5"
			}
			result = result + s
			position--
		}
		if !matchFound {
			result = result + "5"
		}
	} else {
		result = "-"
		str = str[1:]
		position := len(str)
		matchFound := false
		for _, r := range str {
			s := string(r)
			i, _ := strconv.Atoi(s)
			if !matchFound && i >= 5 {
				matchFound = true
				result = result + "5"
			}
			result = result + s
			position--
		}
		if !matchFound {
			result = result + "5"
		}
	}
	output, _ := strconv.Atoi(result)
	return output
}
