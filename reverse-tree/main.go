package main

import (
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	root := &Node{1, nil, nil}
	generateStaticTree(root)
	//generateRandomBinaryTree(root, 1)
	root.PrintLeaves()
	println("after reverse:")
	root.ReverseTree()
	root.PrintLeaves()
}

func generateRandomBinaryTree(n *Node, totalItems int) {
	n.left = &Node{rand.Intn(100), nil, nil}
	n.right = &Node{rand.Intn(100), nil, nil}

	if totalItems > 10 {
		return
	}
	totalItems++

	generateRandomBinaryTree(n.left, totalItems)
	generateRandomBinaryTree(n.right, totalItems)
}

func generateStaticTree(n *Node) {
	n.left = &Node{2, nil, nil}
	n.right = &Node{3, nil, nil}

	n.left.left = &Node{4, nil, nil}
	n.left.right = &Node{5, nil, nil}

	n.right.left = &Node{6, nil, nil}
	n.right.right = &Node{7, nil, nil}
}
