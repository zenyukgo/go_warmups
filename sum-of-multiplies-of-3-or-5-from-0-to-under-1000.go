package main

import (
	"fmt"
)

func main() {
	fmt.Println("sum of multiplies of 3 or 5 from 0 to under 1000")
	threes := make(map[int]struct{})
	fives := make(map[int]struct{})
	i := 3
	for i < 1000 {
		threes[i] = struct{}{}
		i += 3
	}
	i = 5
	for i < 1000 {
		fives[i] = struct{}{}
		i += 5
	}
	for k, _ := range fives {
		threes[k] = struct{}{}
	}
	sum := 0
	for k, _ := range threes {
		sum += k
	}
	fmt.Printf("all: %v \n", sum)
}